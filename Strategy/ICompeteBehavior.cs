﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kata07_EventCompetitors.Strategy
{
    public interface ICompeteBehavior
    {
        void Compete(string name, List<string> register);
    }
}
