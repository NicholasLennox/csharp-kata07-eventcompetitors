﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kata07_EventCompetitors.Strategy
{
    public class CompeteInRun : ICompeteBehavior
    {
        public void Compete(string name, List<string> register)
        {
            register.Add(name + " is competing in the run");
        }
    }
}
