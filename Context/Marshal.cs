﻿using Kata07_EventCompetitors.Strategy;
using System;
using System.Collections.Generic;
using System.Text;

namespace Kata07_EventCompetitors.Context
{
    public class Marshal : Attendee
    {
        public Marshal(string name, ICompeteBehavior competeBehaviour) : base(name, competeBehaviour)
        {

        }
        public override string Render()
        {
            return "Hi, my name is " + this.Name + " and I am a marshal";
        }
    }
}
