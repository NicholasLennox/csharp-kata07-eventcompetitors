﻿using System;
using System.Collections.Generic;
using System.Text;
using Kata07_EventCompetitors.Strategy;

namespace Kata07_EventCompetitors.Context
{
    public abstract class Attendee
    {
        public string Name { get; set; }
        // Reference to strategy
        public ICompeteBehavior CompeteBehavior { get; set; }

        protected Attendee(string name, ICompeteBehavior competeBehavior)
        {
            Name = name;
            CompeteBehavior = competeBehavior;
        }

        // Calling the compete behavior
        public void Compete(List<string> register)
        {
            this.CompeteBehavior.Compete(this.Name, register);
        }

        // Non-compete behaviour: render the attendee
        public abstract string Render();
    }
}
