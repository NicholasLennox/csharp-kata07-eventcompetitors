﻿using Kata07_EventCompetitors.Client;
using Kata07_EventCompetitors.Context;
using Kata07_EventCompetitors.Strategy;
using System;
using System.Collections.Generic;

namespace Kata07_EventCompetitors
{
    class Program
    {
        static void Main(string[] args)
        {
            Event myEvent = new Event()
            {
                Name = "Iron man",
                Attendees = new List<Attendee>()
            };
            // Add new attendee to event, this attendee is an athlete named bob who will compete in the trialthon
            myEvent.Attendees.Add(new Athlete("Bob", new CompeteInTri()));
            myEvent.Attendees.Add(new Marshal("Clive", new NoCompete()));
            myEvent.Attendees.Add(new Athlete("Dewald", new CompeteInRun()));
            myEvent.Attendees.Add(new Athlete("Nick", new CompeteInSwim()));


            // Executing the event
            List<string> greetings = myEvent.RenderAttendees();
            List<string> register = myEvent.Compete();

            foreach (string item in greetings)
            {
                Console.WriteLine(item);
            }
            foreach (string item in register)
            {
                Console.WriteLine(item);
            }
        }
    }
}
