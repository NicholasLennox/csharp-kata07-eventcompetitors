﻿using System;
using System.Collections.Generic;
using System.Text;
using Kata07_EventCompetitors.Context;

namespace Kata07_EventCompetitors.Client
{
    public class Event
    {
        public string Name { get; set; }
        public List<Attendee> Attendees { get; set; }

        // Calling compete behavior, to add competitors to the list
        public List<string> Compete()
        {
            List<string> register = new List<string>();
            foreach (Attendee att in Attendees)
            {
                att.Compete(register);
            }
            return register;
        }

        // Getting all attendees to say hello
        public List<string> RenderAttendees()
        {
            List<string> renders = new List<string>();
            foreach (Attendee attendee in Attendees)
            {
                renders.Add(attendee.Render());
            }
            return renders;
        }
    }
}
